#include "CommandDispatcher.h"

#include "CommandInfo.h"
#include "GridFrameBase.h"
#include "DlgCreateLoadSessionHTML.h"
#include "DlgLicenseMessageBoxHTML.h"
#include "LoggerService.h"
#include "MainFrame.h"
#include "ModuleChannel.h"
#include "ModuleClassMembers.h"
#include "ModuleColumns.h"
#include "ModuleContact.h"
#include "ModuleConversation.h"
#include "ModuleDirectoryAudits.h"
#include "ModuleDirectoryRoles.h"
#include "ModuleDrive.h"
#include "ModuleEduUsers.h"
#include "ModuleEvent.h"
#include "ModuleGroup.h"
#include "ModuleLists.h"
#include "ModuleLicense.h"
#include "ModuleListsItems.h"
#include "ModuleMailboxPermissions.h"
#include "ModuleMessage.h"
#include "ModuleMessageRules.h"
#include "ModuleO365Reports.h"
#include "ModulePrivateChannelMembers.h"
#include "ModulePost.h"
#include "ModuleSchools.h"
#include "ModuleSignIns.h"
#include "ModuleSites.h"
#include "ModuleSitePermissions.h"
#include "ModuleSiteRoles.h"
#include "ModuleSpGroups.h"
#include "ModuleSpUsers.h"
#include "ModuleTeamChannelMessages.h"
#include "ModuleUser.h"
#include "MSGraphSession.h"
#include "Office365Admin.h"
#include "SessionTypes.h"
#include "YCallbackMessage.h"
#include "YtriaTranslate.h"
#include "ModuleApplications.h"
#include "SessionLoaderFactory.h"
#include "SessionSwitcher.h"
#include "ModuleOnPremiseUsers.h"
#include "ModuleOnPremiseGroups.h"
#include "TryLoadSnapshotCommand.h"

void CommandDispatcher::Execute(const Command& p_Command)
{
	if (p_Command.GetTask() == Command::ModuleTask::TryLoadSnapshot)
	{
		ASSERT(p_Command.GetCommandInfo().Data().is_type<std::shared_ptr<TryLoadSnapshotCommand>>());
		auto tlsc = p_Command.GetCommandInfo().Data().get_value<std::shared_ptr<TryLoadSnapshotCommand>>();
		ASSERT(tlsc);
		if (tlsc)
		{
			tlsc->SetParentCommandForNextExecute(&p_Command);
			tlsc->Execute();
		}
		return;
	}

	MainFrame* mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame);

	if (nullptr != mainFrame)
	{
		Office365AdminApp* app = dynamic_cast<Office365AdminApp*>(::AfxGetApp());

		// FIXME: We should check license after having ensured there's a session.

		IF_LICENSE_IS_NONE_AND_MODAL_DEMO_DLG_NOT_OK(p_Command.GetLicenseContext(), p_Command.GetSourceCWnd())
		{
			wstring errMsg;
			const auto errorStatus = mainFrame->GetLicenseValidatorError(errMsg);

			HWND hwnd = p_Command.GetSourceWindow().GetHWND();
			if (nullptr == hwnd && nullptr != p_Command.GetCommandInfo().GetFrame())
				hwnd = p_Command.GetCommandInfo().GetFrame()->GetSafeHwnd();

			if (ILicenseValidator::ErrorStatus::STATUSOK == errorStatus)// general error
			{
				errMsg = YtriaTranslate::Do(CommandDispatcher_Execute_8, _YLOC("No valid license<BR>Canceling command: %1"), p_Command.ToString().c_str());// not using _YFORMATERROR on purpose
				LoggerService::User(errMsg, hwnd);
			}
			else // license feature error
			{
				LoggerService::User(YtriaTranslate::Do(CommandDispatcher_Execute_9, _YLOC("License validation error: %1<BR>Canceling command %2"), errMsg.c_str(), p_Command.ToString().c_str()), hwnd);// not using _YFORMATERROR on purpose
			}
		}
		else
		{
			auto graphSession = Sapio365Session::Find(p_Command.GetSessionIdentifier());

			ASSERT(!graphSession ||
					graphSession->GetMainMSGraphSession()->GetAuthenticationState() == graphSession->GetMSGraphSession(Sapio365Session::APP_SESSION)->GetAuthenticationState() &&
					graphSession->GetMainMSGraphSession()->GetAuthenticationState() == graphSession->GetMSGraphSession(Sapio365Session::USER_SESSION)->GetAuthenticationState());

			if (graphSession && RestSession::AuthenticationState::Ready == graphSession->GetMainMSGraphSession()->GetAuthenticationState())
			{
				auto cmd = p_Command; // Copy command to update context
				auto context = cmd.GetLicenseContext();
				context.SetSessionReferenceForTokenCharging(graphSession->GetIdentifier().Serialize());
				cmd.SetLicenseContext(context);
				ExecuteImpl(cmd);
			}
			else
			{
				if (AutomatedApp::IsAutomationRunning())
				{
					auto action = app->GetCurrentAction();
					ASSERT(nullptr != action);
					if (nullptr != action)
					{
						action->AddError(_YERROR("No session loaded."));
						app->SetAutomationGreenLight(action);
					}
				}
				else
				{
					if (nullptr != graphSession && RestSession::AuthenticationState::Ready != graphSession->GetMainMSGraphSession()->GetAuthenticationState())
					{
						// Session has been interrupted? Don't try to relog automatically
						return;
					}

					// Should only happen for MainFrame session.
					ASSERT(graphSession == mainFrame->GetSapio365Session());

					// We shouldn't reach this point for a "write" command.
					ASSERT(p_Command.GetTask() != Command::ModuleTask::UpdateModified
						&& p_Command.GetTask() != Command::ModuleTask::UpdateGroupItems
						&& p_Command.GetTask() != Command::ModuleTask::UpdateRecycleBin
						&& p_Command.GetTask() != Command::ModuleTask::UpdateLCP
						&& p_Command.GetTask() != Command::ModuleTask::UpdateLCPGroup
						&& p_Command.GetTask() != Command::ModuleTask::UpdateSettings
					);

					auto command = std::make_shared<Command>(p_Command);

					bool noUltraAdminSessionAllowed = false;

					if (p_Command.GetCommandInfo().GetOrigin() != Origin::NotSet)
						noUltraAdminSessionAllowed = p_Command.GetCommandInfo().GetOrigin() == Origin::User;

					// FIXME: handle roles here !
					ASSERT(p_Command.GetSessionIdentifier().m_RoleID == SessionIdentifier::g_NoRole);

					auto sessions = SessionsSqlEngine::Get().GetList();
					if (!CRMpipe().IsFeatureSandboxed())
					{
						auto isUltraAdmin = [](const PersistentSession& p_Session) {
							return p_Session.IsUltraAdmin();
						};
						sessions.erase(std::remove_if(sessions.begin(), sessions.end(), isUltraAdmin), sessions.end());
					}
					DlgCreateLoadSessionHTML dlg(sessions, noUltraAdminSessionAllowed);
					if (dlg.DoModal() == IDOK)
					{
						if (dlg.ShouldCreateStandardSession())
						{
							app->CreateStandardSession(*command);
						}
						else if (dlg.ShouldCreateAdvancedSession())
						{
							app->CreateAdvancedSession(*command);
						}
						else if (dlg.ShouldCreateUltraAdminSession())
						{
							app->CreateUltraAdminSession(_YTEXT(""), _YTEXT(""), _YTEXT(""), _YTEXT(""), *command, nullptr);
						}
						else
						{
							ASSERT(!dlg.ShouldCreateStandardSession() && !dlg.ShouldCreateAdvancedSession() && !dlg.ShouldCreateUltraAdminSession());

							auto sessionId = dlg.GetSelectedSession();

							ASSERT(nullptr != app);
							if (nullptr != app)
							{
								auto loader = SessionLoaderFactory(sessionId).CreateLoader();
								SessionSwitcher(std::move(loader)).Switch().Then([this, command, sessionTechName = sessionId.m_EmailOrAppId, app, mainFrame](const std::shared_ptr<Sapio365Session>& p_Session)
								{
									if (nullptr != p_Session)
									{
										// In case refresh token was expired or session logged-out, wait for authentication to complete
										auto graphSession = app->GetSapioSessionBeingLoaded();
										ASSERT(graphSession);
										const auto state = graphSession->GetMainMSGraphSession()->WaitAuthenticationState();
										// In case of Role Delegation, wait for all 3 sessions to be sure.
										// Not clear if there's any risk not doing this...
										// (If no role, three GetMSGraphSession() calls returns the same one and only session.
										{
											const auto state1 = graphSession->GetMSGraphSession(Sapio365Session::APP_SESSION)->WaitAuthenticationState();
											const auto state2 = graphSession->GetMSGraphSession(Sapio365Session::USER_SESSION)->WaitAuthenticationState();
											ASSERT(state == state1 && state1 == state2);
										}

										if (RestSession::AuthenticationState::Ready == state)
										{
											ASSERT(nullptr != mainFrame);
											if (nullptr != mainFrame)
											{
												mainFrame->WaitSyncCacheEvent();
												command->SetLicenseContext(mainFrame->GetLicenseContext());
											}

											YCallbackMessage::DoPost([this, command]()
												{
													Execute(*command);//  not ExecuteImpl so that new license context can be validated
												});
										}
										else
											LoggerService::Debug(_YTEXTFORMAT(L"CommandManager::Execute: Unable to load session %s", sessionTechName.c_str()).c_str());
									}
									else
										LoggerService::Debug(_YTEXTFORMAT(L"CommandManager::Execute: Unable to load session %s", sessionTechName.c_str()).c_str());
								});
							}
						}
					}
				}			
			}
		}
	}
}

void CommandDispatcher::ExecuteImpl(const Command& p_Command)
{
	ModuleBase* module = getModule(p_Command.GetSessionIdentifier(), p_Command.GetTarget());
	ASSERT(nullptr != module);
	if (nullptr != module)
	{
		bool exe = true;

		if (AutomatedApp::IsAutomationRunning())
		{
			AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
			ASSERT(nullptr != app);
			if (nullptr != app)
			{
				auto currentAction = app->GetCurrentAction();
				if (nullptr != currentAction && currentAction != p_Command.GetAutomationAction())
				{
					// user tried to start something else while automation is running
					exe = false;
					YCallbackMessage::DoSend([p_Command, app, parent = p_Command.GetSourceCWnd()]()
					{
						YCodeJockMessageBox dlg(parent,
												DlgMessageBox::eIcon_ExclamationWarning,
												YtriaTranslate::Do(CommandDispatcher_ExecuteImpl_1, _YLOC("This command cannot be executed at this time.")).c_str(),
												YtriaTranslate::Do(AutomationWizard_Run_2, _YLOC("Please wait for the current task to complete and try again.")).c_str(),
												_YTEXT(""),
												{ { IDCANCEL, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } });
						dlg.SetBlockAutomation(true);
						dlg.DoModal();
					});
				}
			}
		}

		if (exe)
			module->Execute(p_Command);
	}
	else if (AutomatedApp::IsAutomationRunning())
	{
		AutomatedApp* app = dynamic_cast<AutomatedApp*>(AfxGetApp());
		ASSERT(nullptr != app);
		if (nullptr != app)
		{
			ASSERT(app->GetCurrentAction() != nullptr);
			if (app->GetCurrentAction() != nullptr)
			{
				app->GetCurrentAction()->AddError(YtriaTranslate::Do(CommandDispatcher_ExecuteImpl_4, _YLOC("No module found for this command")).c_str());
				app->SetAutomationGreenLight(app->GetCurrentAction());
			}
		}
	}
}

namespace
{
	class FakeModule : public ModuleBase
	{
	public:
		using ModuleBase::ModuleBase;

	private:
		virtual void executeImpl(const Command& p_Command) override {}
	};
}

CommandDispatcher::CommandDispatcher()
	: m_DefaultModule(std::make_unique<FakeModule>(_YTEXT("/dev/null")))
{
}

CommandDispatcher& CommandDispatcher::GetInstance()
{
	static CommandDispatcher g_Instance;
	return g_Instance;
}

ModuleBase* CommandDispatcher::getModule(const SessionIdentifier& p_SessionIdentifier, Command::ModuleTarget p_Target)
{
	ASSERT(!p_SessionIdentifier.m_EmailOrAppId.empty());
	ModulesPerTarget& sessionModuleMap = m_Modules[p_SessionIdentifier];
	auto findIt = sessionModuleMap.find(p_Target);
	if (findIt == sessionModuleMap.end())
	{
        wstring moduleName = getModuleName(p_Target);
		HWND mainframeHwnd = 0;
		CWnd* mainFrame = ::AfxGetMainWnd();
		ASSERT(nullptr != mainFrame);
		if (nullptr != mainFrame)
			mainframeHwnd = ::AfxGetMainWnd()->GetSafeHwnd();
        LoggerService::Debug(_YTEXTFORMAT(L"CommandManager::getModule: Opening module %s for session %s", moduleName.c_str(), p_SessionIdentifier.m_EmailOrAppId.c_str()), mainframeHwnd);
		switch (p_Target)
		{
			case Command::ModuleTarget::ClassMembers:
				sessionModuleMap[p_Target] = std::make_unique<ModuleClassMembers>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Contact:
                sessionModuleMap[p_Target] = std::make_unique<ModuleContact>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::Conversation:
                sessionModuleMap[p_Target] = std::make_unique<ModuleConversation>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::DirectoryRoles:
                sessionModuleMap[p_Target] = std::make_unique<ModuleDirectoryRoles>(moduleName);
                return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Drive:
                sessionModuleMap[p_Target] = std::make_unique<ModuleDrive>(moduleName);
                return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Group:
				sessionModuleMap[p_Target] = std::make_unique<ModuleGroup>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Mail:
				ASSERT(false);// TODO
			case Command::ModuleTarget::Message:
                sessionModuleMap[p_Target] = std::make_unique<ModuleMessage>(moduleName);
                return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Organization:
				ASSERT(false);// TODO
			case Command::ModuleTarget::User:
				sessionModuleMap[p_Target] = std::make_unique<ModuleUser>(moduleName);
				return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::Event:
                sessionModuleMap[p_Target] = std::make_unique<ModuleEvent>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::MessageRules:
				sessionModuleMap[p_Target] = std::make_unique<ModuleMessageRules>(moduleName);
				return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::Sites:
                sessionModuleMap[p_Target] = std::make_unique<ModuleSites>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::Lists:
                sessionModuleMap[p_Target] = std::make_unique<ModuleLists>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::ListsItem:
                sessionModuleMap[p_Target] = std::make_unique<ModuleListsItems>(moduleName);
                return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Licenses:
				sessionModuleMap[p_Target] = std::make_unique<ModuleLicense>(moduleName);
				return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::Post:
                sessionModuleMap[p_Target] = std::make_unique<ModulePost>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::Columns:
                sessionModuleMap[p_Target] = std::make_unique<ModuleColumns>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::Channel:
                sessionModuleMap[p_Target] = std::make_unique<ModuleChannel>(moduleName);
                return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::ChannelMessages:
				sessionModuleMap[p_Target] = std::make_unique<ModuleTeamChannelMessages>(moduleName);
				return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::SpUser:
                sessionModuleMap[p_Target] = std::make_unique<ModuleSpUsers>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::SpGroup:
                sessionModuleMap[p_Target] = std::make_unique<ModuleSpGroups>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::SitePermissions:
                sessionModuleMap[p_Target] = std::make_unique<ModuleSitePermissions>(moduleName);
                return sessionModuleMap[p_Target].get();
            case Command::ModuleTarget::SiteRoles:
                sessionModuleMap[p_Target] = std::make_unique<ModuleSiteRoles>(moduleName);
                return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Reports:
				sessionModuleMap[p_Target] = std::make_unique<ModuleO365Reports>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::SignIns:
				sessionModuleMap[p_Target] = std::make_unique<ModuleSignIns>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::DirectoryAudit:
				sessionModuleMap[p_Target] = std::make_unique<ModuleDirectoryAudits>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Schools:
				sessionModuleMap[p_Target] = std::make_unique<ModuleSchools>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::EduUsers:
				sessionModuleMap[p_Target] = std::make_unique<ModuleEduUsers>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::ChannelMembers:
				sessionModuleMap[p_Target] = std::make_unique<ModulePrivateChannelMembers>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::Applications:
				sessionModuleMap[p_Target] = std::make_unique<ModuleApplications>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::MailboxPermissions :
				sessionModuleMap[p_Target] = std::make_unique<ModuleMailboxPermissions>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::OnPremiseUsers:
				sessionModuleMap[p_Target] = std::make_unique<ModuleOnPremiseUsers>(moduleName);
				return sessionModuleMap[p_Target].get();
			case Command::ModuleTarget::OnPremiseGroups:
				sessionModuleMap[p_Target] = std::make_unique<ModuleOnPremiseGroups>(moduleName);
				return sessionModuleMap[p_Target].get();
			default:
                ASSERT(false);
                LoggerService::Debug(_YTEXTFORMAT(L"CommandManager::getModule: Tried to open module %s for session %s, but it does not exist!", moduleName.c_str(), p_SessionIdentifier.m_EmailOrAppId.c_str()));
				break;
		}
	}
	else
		return findIt->second.get();

	return m_DefaultModule.get();
}

const CommandDispatcher::ModulesPerSession& CommandDispatcher::GetAllModules() const
{
	return m_Modules;
}

wstring CommandDispatcher::getModuleName(Command::ModuleTarget target)
{
	switch (target)
	{
	case Command::ModuleTarget::ClassMembers:
		return _T("Class Members");

	case Command::ModuleTarget::Contact:
		return YtriaTranslate::Do(Command_ToString_1, _YLOC("Contact")).c_str();

    case Command::ModuleTarget::Conversation:
        return YtriaTranslate::Do(Command_ToString_2, _YLOC("Conversation")).c_str();

    case Command::ModuleTarget::DirectoryRoles:
        return YtriaTranslate::Do(CommandDispatcher_getModuleName_4, _YLOC("Directory Roles")).c_str();

	case Command::ModuleTarget::Drive:
		return YtriaTranslate::Do(CommandDispatcher_getModuleName_5, _YLOC("Drives")).c_str();

	case Command::ModuleTarget::Event:
		return YtriaTranslate::Do(CommandDispatcher_getModuleName_6, _YLOC("Event")).c_str();

	case Command::ModuleTarget::Group:
		return YtriaTranslate::Do(Command_ToString_6, _YLOC("Group")).c_str();

	case Command::ModuleTarget::Mail:
		return YtriaTranslate::Do(Command_ToString_7, _YLOC("Mail")).c_str();

	case Command::ModuleTarget::Message:
		return YtriaTranslate::Do(Command_ToString_8, _YLOC("Message")).c_str();

	case Command::ModuleTarget::Organization:
		return YtriaTranslate::Do(Command_ToString_9, _YLOC("Organization")).c_str();

	case Command::ModuleTarget::User:
		return YtriaTranslate::Do(Command_ToString_10, _YLOC("User")).c_str();

    case Command::ModuleTarget::Sites:
        return YtriaTranslate::Do(Command_ToString_12, _YLOC("Sites")).c_str();

    case Command::ModuleTarget::Lists:
        return YtriaTranslate::Do(Command_ToString_13, _YLOC("Lists")).c_str();

    case Command::ModuleTarget::ListsItem:
        return YtriaTranslate::Do(Command_ToString_14, _YLOC("List Items")).c_str();

	case Command::ModuleTarget::Licenses:
		return YtriaTranslate::Do(Command_ToString_15, _YLOC("Licenses")).c_str();

    case Command::ModuleTarget::Post:
        return YtriaTranslate::Do(CommandDispatcher_getModuleName_17, _YLOC("Posts")).c_str();

    case Command::ModuleTarget::Columns:
        return YtriaTranslate::Do(CommandDispatcher_getModuleName_18, _YLOC("Columns")).c_str();

    case Command::ModuleTarget::Channel:
        return YtriaTranslate::Do(CommandDispatcher_getModuleName_19, _YLOC("Channels")).c_str();

	case Command::ModuleTarget::MessageRules:
		return YtriaTranslate::Do(CommandDispatcher_getModuleName_20, _YLOC("Message Rules")).c_str();

	case Command::ModuleTarget::ChannelMessages:
		return YtriaTranslate::Do(CommandDispatcher_getModuleName_21, _YLOC("Channel Messages")).c_str();

    case Command::ModuleTarget::SpUser:
        return YtriaTranslate::Do(CommandDispatcher_getModuleName_23, _YLOC("Site Users")).c_str();

    case Command::ModuleTarget::SpGroup:
        return YtriaTranslate::Do(CommandDispatcher_getModuleName_24, _YLOC("Site Groups")).c_str();

    case Command::ModuleTarget::SitePermissions:
        return YtriaTranslate::Do(CommandDispatcher_getModuleName_25, _YLOC("Site Permissions")).c_str();

    case Command::ModuleTarget::SiteRoles:
        return YtriaTranslate::Do(CommandDispatcher_getModuleName_26, _YLOC("Site Roles")).c_str();

	case Command::ModuleTarget::Reports:
		return YtriaTranslate::Do(CommandDispatcher_getModuleName_27, _YLOC("Reports")).c_str();

	case Command::ModuleTarget::SignIns:
		return YtriaTranslate::Do(CommandDispatcher_getModuleName_28, _YLOC("Sign-Ins")).c_str();

	case Command::ModuleTarget::DirectoryAudit:
		return _T("Audit Logs");

	case Command::ModuleTarget::Schools:
		return _T("Schools");

	case Command::ModuleTarget::EduUsers:
		return _T("Education Users");

	case Command::ModuleTarget::ChannelMembers:
		return _T("Channel Members");

	case Command::ModuleTarget::Applications:
		return _T("Applications");

	case Command::ModuleTarget::MailboxPermissions:
		return _T("Mailbox Permissions");

	case Command::ModuleTarget::OnPremiseUsers:
		return _T("On-Premises Users");

	case Command::ModuleTarget::OnPremiseGroups:
		return _T("On-Premises Groups");

	case Command::ModuleTarget::numModuleTargets:
	default:
		break;
	}

	ASSERT(false);
	return _YTEXT("");
}

vector<GridFrameBase*> CommandDispatcher::GetAllGridFrames()
{
	vector<GridFrameBase*> gridFrames;

	for (const auto& byUserItem : m_Modules)
	{
		for (const auto& item : byUserItem.second)
		{
			ModuleBase* module = item.second.get();
			auto moduleFrames = module->GetGridFrames();

			std::copy(moduleFrames.begin(), moduleFrames.end(), std::inserter(gridFrames, gridFrames.end()));
		}
	}

	return gridFrames;
}