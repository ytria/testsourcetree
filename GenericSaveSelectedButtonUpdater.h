#pragma once

#include "IButtonUpdater.h"

class GridFrameBase;

class GenericSaveSelectedButtonUpdater : public IButtonUpdater
{
public:
	GenericSaveSelectedButtonUpdater(GridFrameBase& p_Frame);
	void Update(CCmdUI* p_CmdUi) override;

private:
	GridFrameBase& m_Frame;
};

