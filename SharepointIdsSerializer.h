#pragma once

#include "IJsonSerializer.h"

#include "SharePointIds.h"

class SharepointIdsSerializer : public IJsonSerializer
{
public:
    SharepointIdsSerializer(const SharepointIds& p_SpIds);
    void Serialize() override;

private:
    const SharepointIds& m_SpIds;
};

