#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessDirectoryRoleTemplate.h"

class DirectoryRoleTemplateDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessDirectoryRoleTemplate>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

