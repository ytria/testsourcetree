#pragma once

#include "IRequester.h"
#include "EducationClass.h"
#include "ValueListDeserializer.h"
#include "IPageRequestLogger.h"

class EducationClass;
class EducationClassDeserializer;

class EducationClassListRequester : public IRequester
{
public:
	EducationClassListRequester(const std::shared_ptr<IPageRequestLogger>& p_Logger);
	EducationClassListRequester(const wstring& p_SchoolId, const std::shared_ptr<IPageRequestLogger>& p_Logger);
	TaskWrapper<void> Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData) override;
	const vector<EducationClass>& GetData() const;

private:
	std::shared_ptr<ValueListDeserializer<EducationClass, EducationClassDeserializer>> m_Deserializer;
	wstring m_SchoolId;
	std::shared_ptr<IPageRequestLogger> m_Logger;
};

