#pragma once

#include "BusinessUser.h"

// only for identification issues in grids
class BusinessUserDeleted : public BusinessUser
{
public:
	using BusinessUser::BusinessUser;

private:
	RTTR_ENABLE(BusinessUser)
};
