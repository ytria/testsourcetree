#pragma once

#include "IButtonUpdater.h"

class GridFrameBase;

class GenericRevertAllButtonUpdater : public IButtonUpdater
{
public:
	GenericRevertAllButtonUpdater(GridFrameBase& p_Frame);
	void Update(CCmdUI* p_CmdUi) override;

private:
	GridFrameBase& m_Frame;
};

