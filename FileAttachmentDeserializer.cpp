#include "FileAttachmentDeserializer.h"

#include "JsonSerializeUtil.h"

void FileAttachmentDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeString(_YTEXT("contentBytes"), m_Data.m_ContentBytes, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("contentId"), m_Data.m_ContentId, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("contentLocation"), m_Data.m_ContentLocation, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("contentType"), m_Data.m_ContentType, p_Object);
	JsonSerializeUtil::DeserializeId(_YTEXT("id"), m_Data.m_Id, p_Object, true);
    JsonSerializeUtil::DeserializeBool(_YTEXT("isInline"), m_Data.m_IsInline, p_Object);
    JsonSerializeUtil::DeserializeTimeDate(_YTEXT("lastModifiedDateTime"), m_Data.m_LastModifiedDateTime, p_Object);
    JsonSerializeUtil::DeserializeString(_YTEXT("name"), m_Data.m_Name, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("size"), m_Data.m_Size, p_Object);
}
