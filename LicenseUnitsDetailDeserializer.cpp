#include "LicenseUnitsDetailDeserializer.h"

#include "JsonSerializeUtil.h"

void LicenseUnitsDetailDeserializer::DeserializeObject(const web::json::object& p_Object)
{
    JsonSerializeUtil::DeserializeInt32(_YTEXT("enabled"), m_Data.m_Enabled, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("suspended"), m_Data.m_Suspended, p_Object);
    JsonSerializeUtil::DeserializeInt32(_YTEXT("warning"), m_Data.m_Warning, p_Object);
}
