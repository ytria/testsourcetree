#include "PSSerializeUtil.h"
#include "IPSObjectPropertyReader.h"
#include "YtriaPowerShellDll.h"
#include "TimeUtil.h"

wstring PSSerializeUtil::DeserializeString(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	wstring result;

	if (p_Reader.HasProperty(p_PropName.c_str()))
		result = p_Reader.GetStringProperty(p_PropName.c_str());

	return result;
}

vector<uint8_t> PSSerializeUtil::DeserializeByteArray(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	vector<uint8_t> result;

	if (p_Reader.HasProperty(p_PropName.c_str()))
	{
		wstring base64Result = p_Reader.GetByteArrayProperty(p_PropName.c_str());
		result = utility::conversions::from_base64(base64Result);
	}

	return result;
}

vector<PooledString> PSSerializeUtil::DeserializeStringCollection(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	vector<PooledString> result;

	if (p_Reader.HasProperty(p_PropName.c_str()))
	{
		auto collection = p_Reader.GetCollectionProperty(p_PropName.c_str());
		result = Str::explodeIntoVector<PooledString>(collection->GetStringValues(_YTEXT("\n")), _YTEXT("\n"));
		YtriaPowerShellDll::Get().ReleaseCollectionProperty(collection);
	}

	return result;
}

vector<YTimeDate> PSSerializeUtil::DeserializeDateTimeCollection(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	vector<YTimeDate> result;

	if (p_Reader.HasProperty(p_PropName.c_str()))
	{
		auto collection = p_Reader.GetCollectionProperty(p_PropName.c_str());
		vector<wstring> values = Str::explodeIntoVector<wstring>(collection->GetDateTimeValues(_YTEXT("\n")), _YTEXT("\n"));
		for (const auto& val : values)
		{
			int64_t intDateTime = std::stoll(val);
			auto dateTime = DeserializeDateTime(intDateTime);
			if (!dateTime)
				dateTime = GridBackendUtil::g_NoValueDate;
			result.push_back(*dateTime);
		}
		YtriaPowerShellDll::Get().ReleaseCollectionProperty(collection);
	}

	return result;
}

bool PSSerializeUtil::DeserializeBool(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	bool result{};

	if (p_Reader.HasProperty(p_PropName.c_str()))
		result = p_Reader.GetBoolProperty(p_PropName.c_str());

	return result;
}

int32_t PSSerializeUtil::DeserializeInt32(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	int32_t result{};

	if (p_Reader.HasProperty(p_PropName.c_str()))
		result = p_Reader.GetInt32Property(p_PropName.c_str());

	return result;
}

int64_t PSSerializeUtil::DeserializeInt64(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	int64_t result{};

	if (p_Reader.HasProperty(p_PropName.c_str()))
		result = p_Reader.GetInt64Property(p_PropName.c_str());

	return result;
}

boost::YOpt<YTimeDate> PSSerializeUtil::DeserializeInt64AsTimeDate(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	boost::YOpt<YTimeDate> result;

	if (p_Reader.HasProperty(p_PropName.c_str()))
	{
		int64_t value = p_Reader.GetInt64Property(p_PropName.c_str());
		result = DeserializeDateTime(value);
	}

	return result;
}

boost::YOpt<YTimeDate> PSSerializeUtil::DeserializeDateTime(IPSObjectPropertyReader& p_Reader, const wstring& p_PropName)
{
	boost::YOpt<YTimeDate> result;

	if (p_Reader.HasProperty(p_PropName.c_str()))
	{
		int64_t value = p_Reader.GetDateTimeProperty(p_PropName.c_str());
		result = DeserializeDateTime(value);
	}

	return result;
}

boost::YOpt<YTimeDate> PSSerializeUtil::DeserializeDateTime(int64_t p_DateTime)
{
	boost::YOpt<YTimeDate> result;
	if (p_DateTime != 0 && p_DateTime != 0x7fffffffffffffff)
	{
		FILETIME fileTime;
		fileTime.dwHighDateTime = p_DateTime >> 32;
		fileTime.dwLowDateTime = p_DateTime & 0xffffffff;

		SYSTEMTIME systemTime;
		::FileTimeToSystemTime(&fileTime, &systemTime);

		result = YTimeDate(systemTime, 0);
	}

	return result;
}
