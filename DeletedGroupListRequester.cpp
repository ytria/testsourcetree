#include "DeletedGroupListRequester.h"

#include "BusinessGroup.h"
#include "IPropertySetBuilder.h"
#include "MSGraphCommonData.h"
#include "MSGraphUtil.h"
#include "MSGraphSession.h"
#include "PaginatedRequestResults.h"
#include "SQL + Cloud/Role Delegation/RoleDelegationManager.h"
#include "Sapio365Session.h"
#include "GroupDeserializer.h"
#include "GroupDeserializerFactory.h"
#include "PaginatorUtil.h"
#include "ValueListDeserializer.h"
#include "RbacRequiredPropertySet.h"
#include "MsGraphHttpRequestLogger.h"

DeletedGroupListRequester::DeletedGroupListRequester(const IPropertySetBuilder& p_PropertySet, const std::shared_ptr<IPageRequestLogger>& p_Logger)
    : m_Properties(std::move(p_PropertySet.GetPropertySet())),
	m_Logger(p_Logger)
{
}

TaskWrapper<void> DeletedGroupListRequester::Send(std::shared_ptr<const Sapio365Session> p_Session, YtriaTaskData p_TaskData)
{
	auto factory	= std::make_unique<GroupDeserializerFactory>(p_Session);
    m_Deserializer	= std::make_shared<ValueListDeserializer<BusinessGroup, GroupDeserializer>>(std::move(factory));

	// Need to request RBAC props?
	//const auto properties = p_Session->WithRBACRequiredProperties<BusinessGroup>(m_Properties);
	const auto properties = RbacRequiredPropertySet<BusinessGroup>(m_Properties, *p_Session).GetPropertySet();

	web::uri_builder uri(_YTEXT("directory/deletedItems/microsoft.graph.group"));
	uri.append_query(_YTEXT("$select"), Rest::GetSelectQuery(properties));

	auto httpLogger = std::make_shared<MsGraphHttpRequestLogger>(Sapio365Session::USER_SESSION, p_Session->GetIdentifier());
	return Util::CreateDefaultGraphPaginator(uri.to_uri(), m_Deserializer, m_Logger, httpLogger, p_TaskData.GetOriginator()).Paginate(p_Session->GetMSGraphSession(httpLogger->GetSessionType()), p_TaskData);
}

vector<BusinessGroup>& DeletedGroupListRequester::GetData()
{
    return m_Deserializer->GetData();
}

