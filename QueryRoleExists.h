#pragma once

#include "ISqlQuery.h"
#include "ExistsRowHandler.h"

class SessionsSqlEngine;

class QueryRoleExists : public ISqlQuery
{
public:
	QueryRoleExists(int64_t p_RoleId, SessionsSqlEngine& p_Engine);
	void Run() override;
	bool Exists() const;

private:
	int64_t m_RoleId;
	SessionsSqlEngine& m_Engine;
	ExistsRowHandler m_Handler;
};

