#include "UserFullPropertySet.h"

vector<rttr::property> UserFullPropertySet::GetPropertySet() const
{
    // FIXME: Don't use "User" class to do that (we need to eventually get rid of it)
    User dummy;
    return dummy.GetSyncProperties(dummy);
}
