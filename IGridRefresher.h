#pragma once

class IGridRefresher
{
public:
	virtual ~IGridRefresher() = default;

	virtual void RefreshAll() = 0;
	virtual void RefreshSelection() = 0;
	virtual void Sync();
};

