#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "OptionalClaim.h"

class OptionalClaimDeserializer : public JsonObjectDeserializer, public Encapsulate<OptionalClaim>
{
protected:
	void DeserializeObject(const web::json::object& p_Object) override;
};

