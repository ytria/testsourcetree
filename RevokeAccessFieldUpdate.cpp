#include "RevokeAccessFieldUpdate.h"

#include "ActivityLoggerUtil.h"
#include "BaseO365Grid.h"

const wstring& RevokeAccessFieldUpdate::GetPlaceHolder()
{
	const static wstring placeHolder = _T("-- Revoke pending --");
	return placeHolder;
}

RevokeAccessFieldUpdate::RevokeAccessFieldUpdate(O365Grid& p_Grid, const row_pk_t& p_RowKey, const UINT& p_ColumnId, const GridBackendField& p_OldValue, const GridBackendField& p_NewValue)
	: FieldUpdateO365(p_Grid, p_RowKey, p_ColumnId, p_OldValue, p_NewValue)
{
	ASSERT(p_Grid.GetColumnByUniqueID(_YUID(O365_USER_SIGNINSESSIONSVALIDFROMDATETIME))->GetID() == p_ColumnId);
	ASSERT(GetPlaceHolder() == p_NewValue.GetValueStr());
}

void RevokeAccessFieldUpdate::RefreshState()
{
	GridBackendRow* row = getGrid().GetRowIndex().GetExistingRow(GetRowKey());

	ASSERT(nullptr != row);
	if (nullptr != row)
	{
		GridBackendField& gridField = row->GetField(GetColumnID());
		if (!gridField.HasValue())
			getActualValue() = boost::none;
		else
			getActualValue() = gridField;

		if (getActualValue())
			getActualValue()->SetCaseSensitive(true); // Force field comparison to be case-sensitive (as the modified state is).

		if (GetActualValue() == GetNewValue()) // Still shows place holder
		{
			if (row->IsError())
			{
				m_State = State::RemoteError;
			}
			else
			{
				ASSERT(false); // How is this possible?
				m_State = State::RemoteHasNewValue;
			}
		}
		else if (GetActualValue())
		{
			if (GetActualValue()->GetIcon() == GridBackendUtil::ICON_ERROR)
			{
				m_State = State::RemoteError;
			}
			else
			{
				ASSERT(GetActualValue()->IsDate());
				m_State = State::RemoteHasNewValue;
			}
		}
		else
		{
			ASSERT(false); // How is this possible?
			m_State = State::RemoteHasNewValue;
		}
	}
}

vector<Modification::ModificationLog> RevokeAccessFieldUpdate::GetModificationLogs() const
{
	auto column = getGrid().GetColumnByID(GetColumnID());
	ASSERT(nullptr != column);

	// Should action be "RevokeAccess" ?
	return { { GridBackendUtil::ToString(GetRowKey()), GetObjectName(), ActivityLoggerUtil::g_ActionUpdate, getGrid().GetAutomationName(), GetState() } };
}

std::unique_ptr<FieldUpdateO365> RevokeAccessFieldUpdate::CreateReplacement(std::unique_ptr<FieldUpdateO365> p_Replacement)
{
	ASSERT(nullptr != dynamic_cast<RevokeAccessFieldUpdate*>(p_Replacement.get()));
	auto newFU = FieldUpdateO365::CreateReplacement(std::move(p_Replacement));
	ASSERT(nullptr != dynamic_cast<RevokeAccessFieldUpdate*>(newFU.get()));
	return newFU;
}
