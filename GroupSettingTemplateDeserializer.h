#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "BusinessGroupSettingTemplate.h"

class GroupSettingTemplateDeserializer : public JsonObjectDeserializer, public Encapsulate<BusinessGroupSettingTemplate>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;    
};

