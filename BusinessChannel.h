#pragma once

#include "BusinessAADUserConversationMember.h"
#include "BusinessDrive.h"
#include "BusinessObject.h"

class Channel;

class BusinessChannel : public BusinessObject
{
public:
    BusinessChannel() = default;
    BusinessChannel(const Channel& p_Channel);

    const boost::YOpt<PooledString>& GetDescription() const;
    void SetDescription(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<PooledString>& GetDisplayName() const;
	void SetDisplayName(const boost::YOpt<PooledString>& p_Val);

	bool operator<(const BusinessChannel& p_Other) const;

	const boost::YOpt<PooledString>& GetEmail() const;
	void SetEmail(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<bool>& GetIsFavoriteByDefault() const;
	void SetIsFavoriteByDefault(const boost::YOpt<bool>& p_Val);
	
	const boost::YOpt<PooledString>& GetWebUrl() const;
	void SetWebUrl(const boost::YOpt<PooledString>& p_Val);
	
	const boost::YOpt<PooledString>& GetMembershipType() const;
	void SetMembershipType(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<BusinessDrive>& GetDrive() const;
	void SetDrive(const boost::YOpt<BusinessDrive>& p_Val);

	const boost::YOpt<BusinessDriveItem>& GetFilesFolder() const;
	void SetFilesFolder(const boost::YOpt<BusinessDriveItem>& p_Val);

	const boost::YOpt<PooledString>& GetGroupId() const;
	void SetGroupId(const boost::YOpt<PooledString>& p_Val);

	const boost::YOpt<vector<BusinessAADUserConversationMember>>& GetPrivateChannelMembers() const;
	void SetPrivateChannelMembers(const boost::YOpt<vector<BusinessAADUserConversationMember>>& p_Val);

	void SetPrivateChannelMembersError(const boost::YOpt<SapioError>& p_Error);
	const boost::YOpt<SapioError>& GetPrivateChannelMembersError() const;

private:
    boost::YOpt<PooledString> m_Description;
	boost::YOpt<PooledString> m_DisplayName;
	boost::YOpt<PooledString> m_Email;
	boost::YOpt<bool>		  m_IsFavoriteByDefault;
	boost::YOpt<PooledString> m_WebUrl;
	boost::YOpt<PooledString> m_MembershipType;

	// Non properties
	boost::YOpt<PooledString> m_GroupId;
	boost::YOpt<BusinessDrive> m_Drive;
	boost::YOpt<BusinessDriveItem> m_FilesFolder;
	boost::YOpt<vector<BusinessAADUserConversationMember>> m_PrivateChannelMembers;
	boost::YOpt<SapioError>	m_PrivateChannelMembersError;

    RTTR_ENABLE(BusinessObject)
    friend class ChannelDeserializer;
};

DECLARE_BUSINESSOBJECT_EQUALTO_AND_HASH_SPECIALIZATION(BusinessChannel)
