#pragma once

#include "IJsonSerializer.h"
#include "BusinessUser.h"
#include "SqlCacheConfig.h"

class DbUserSerializer : public IJsonSerializer
{
public:
	// Memory Cache
	static const std::vector<PooledString>& MemoryCacheRequestFields();

	// List
	static const std::vector<PooledString>& AssignedLicensesRequestFields(); // assignedlicenses sometimes needed for MemoryCache (for rbac)
	static const std::vector<PooledString>& NotInMemoryCacheListRequestFields();

	// Load more(s)
	static const std::vector<PooledString>& SyncOnlyRequestFields_v1();
	static const std::vector<PooledString>& SyncOnlyRequestFields_beta();
	static const std::vector<PooledString>& SyncOnlyRequestFields_all(); // result is concat of SyncOnlyRequestFields_v1() and SyncOnlyRequestFields_beta()

	static const std::vector<PooledString>& MailboxSettingsRequestFields();
	static const std::vector<PooledString>& ArchiveFolderNameRequestFields();
	static const std::vector<PooledString>& DriveRequestFields();
	static const std::vector<PooledString>& ManagerRequestFields();

	// Powershell
	static const std::vector<PooledString>& MailboxRequestFields();
	static const std::vector<PooledString>& RecipientPermissionsRequestFields();

public:
	DbUserSerializer(const BusinessUser& p_User, const UBI p_BlockId);

    void Serialize() override;

protected:
	DbUserSerializer(const BusinessUser& p_User, const std::vector<PooledString>& p_Fields);

private:
    BusinessUser m_User;
	const std::vector<PooledString>& m_Fields;
	PooledString m_DataDateField;
};

