#include "O365Gateways.h"
#include "MSGraphCommonData.h"
#include "MSGraphSession.h"
#include "NetworkUtils.h"
#include "O365AdminErrorHandler.h"
#include "OAuth2Authenticators.h"
#include "RESTUtils.h"
#include "SessionTypes.h"
#include "RoleDelegationUtil.h"
#include "AppGraphSessionCreator.h"
#include "UserGraphSessionCreator.h"

std::shared_ptr<MSGraphSession> O365Gateways::GetUserGraphSession() const
{
    return m_MsGraphSessionUser;
}

std::shared_ptr<MSGraphSession> O365Gateways::GetAppGraphSession() const
{
    return m_MsGraphSessionApp;
}

void O365Gateways::SetUserGraphSession(const std::shared_ptr<MSGraphSession>& p_Session)
{
	m_MsGraphSessionUser = p_Session;
}

void O365Gateways::SetAppGraphSession(const std::shared_ptr<MSGraphSession>& p_Session)
{
	m_MsGraphSessionApp = p_Session;
}

void O365Gateways::SetTenant(const wstring& p_Tenant)
{
    m_Tenant = p_Tenant;
}

void O365Gateways::SetO365Credentials(const wstring& p_Username, const wstring& p_Password)
{
    m_O365Username = p_Username;
    m_O365Password = p_Password;
}

void O365Gateways::SetAppGraphSessionCredentials(const wstring& p_ClientId, const wstring& p_ClientSecret)
{
    m_UltraAdminSessionClientId		= p_ClientId;
    m_UltraAdminSessionClientSecret = p_ClientSecret;
}

const wstring& O365Gateways::GetUltraAdminAppClientId() const
{
	return m_UltraAdminSessionClientId;
}

void O365Gateways::CreateSessions(const wstring& p_AppRefName)
{
	m_UserSessionCreator = UserGraphSessionCreator(m_Tenant, m_O365Username, m_O365Password);
	m_MsGraphSessionUser = m_UserSessionCreator->Create();

	m_AppSessionCreator = AppGraphSessionCreator(m_Tenant, m_UltraAdminSessionClientId, m_UltraAdminSessionClientSecret, p_AppRefName);
	m_MsGraphSessionApp = m_AppSessionCreator->Create();

    m_AreGraphSessionsValid = m_UserSessionCreator->GetResult().m_Success && m_AppSessionCreator->GetResult().m_Success;
}

void O365Gateways::ClearSessions()
{
    if (nullptr != m_MsGraphSessionUser)
        m_MsGraphSessionUser->ClearToken();

	if (nullptr != m_MsGraphSessionApp)
        m_MsGraphSessionApp->ClearToken();

    m_MsGraphSessionUser.reset();
    m_MsGraphSessionApp.reset();

    m_AreGraphSessionsValid = false;
}

bool O365Gateways::AreGraphSessionsValid() const
{
    return m_AreGraphSessionsValid;
}

const boost::YOpt<UserGraphSessionCreator>& O365Gateways::GetLastUserSessionCreator() const
{
    return m_UserSessionCreator;
}

const boost::YOpt<AppGraphSessionCreator>& O365Gateways::GetLastAppSessionCreator() const
{
    return m_AppSessionCreator;
}
