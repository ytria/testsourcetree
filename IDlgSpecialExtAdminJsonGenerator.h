#pragma once

class DlgSpecialExtAdminAccess;

class IDlgSpecialExtAdminJsonGenerator
{
public:
	virtual ~IDlgSpecialExtAdminJsonGenerator() = default;
	virtual web::json::value Generate(const DlgSpecialExtAdminAccess& p_Dlg) = 0;
};

