#pragma once

class ParentalControlSettings
{
public:
	boost::YOpt<vector<PooledString>> m_CountriesBlockedForMinors;
	boost::YOpt<PooledString> m_LegalAgeGroupRule;
};

