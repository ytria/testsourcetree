#include "LinkHandlerJobSchedule.h"

#include "AutomationPresetMaker.h"
#include "DlgJobPresetEdit.h"
#include "DlgScheduleConfigHTML.h"
#include "MainFrame.h"
#include "Product.h"
#include "WindowsScheduledTask.h"

void LinkHandlerJobSchedule::Handle(YBrowserLink& p_Link) const
{
	auto mainFrame = dynamic_cast<MainFrame*>(AfxGetApp()->m_pMainWnd);
	ASSERT(nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr);
	if (nullptr != mainFrame && mainFrame->GetAutomationWizard() != nullptr)
	{
		auto scriptID = Str::getINT64FromString(p_Link.GetUrl().path().substr(1));// .substr(1): path starts with '/'
		auto s = mainFrame->GetAutomationWizard()->GetScript(scriptID);
		ASSERT(s.HasID());
		ASSERT(s.IsRunnable());
		if (s.HasID())
		{
			wstring taskName = _YTEXTFORMAT(L"sapio365_%s_%s", s.m_Key.c_str(), YTimeDate::GetCurrentTimeDate().GetCOleDateTime().Format(_YTEXT("%Y%m%d%H%M%S")));
			wstring jobtitle = s.m_Title;

			DlgScheduleConfigHTML dlg(taskName, jobtitle, mainFrame);
			if (IDOK == dlg.DoModal())
			{
				boost::YOpt<ScriptPreset> newPreset;

				AutomationPresetMaker maker(s, mainFrame->GetAutomationWizard(), mainFrame);
				const auto& errors = maker.GetErrors();
				if (errors.empty() && !maker.Cancel())
				{
					DlgJobPresetEdit dlgPreset(s, mainFrame);
					if (IDOK == dlgPreset.DoModal())
					{
						newPreset = dlgPreset.GetPreset();
						mainFrame->GetAutomationWizard()->AddScriptPreset(s, maker.GetGeneratedPresetContent(), *newPreset);
					}
					else
						return;

					if (s.m_PresetRequired_vol && !newPreset.get().HasID())
						return;// a preset was needed but there was a creation error or cancellation
				}
				else
					return;

				const auto& taskSchedule = dlg.GetTaskSchedule();
				ASSERT(taskSchedule);
				if (taskSchedule)
				{
					taskName = dlg.GetDisplayName();
					const wstring commandLine = newPreset ?
						_YTEXTFORMAT(L"%s -j \"%s\" -pj \"%s\"", (Product::getInstance().getModulePath() + Product::getInstance().getModuleName()).c_str(), s.m_Key.c_str(), newPreset.get().m_Key.c_str()) :
						_YTEXTFORMAT(L"%s -j \"%s\"", (Product::getInstance().getModulePath() + Product::getInstance().getModuleName()).c_str(), s.m_Key.c_str());

					const auto error = WindowsScheduledTask::Create(taskName, commandLine, *taskSchedule);
					if (error.empty())
					{
						ScriptSchedule sps;
						sps.m_Title = dlg.GetDisplayName();
						sps.m_Key = taskName;
						sps.m_Description = taskSchedule->GetDescription();
						if (newPreset)
							mainFrame->GetAutomationWizard()->AddScriptSchedule(s, newPreset.get(), sps);
						else
							mainFrame->GetAutomationWizard()->AddScriptSchedule(s, sps);
					}
					else
						YCodeJockMessageBox(mainFrame,
							DlgMessageBox::eIcon_Error,
							_YTEXT("Error"),
							_YTEXTFORMAT(L"Failed to create the Windows Scheduler Task \"%s\"", taskName.c_str()),
							error,
							{ { IDOK, _YTEXT("OK") } }).DoModal();
				}
			}
		}
		else
			YCodeJockMessageBox(mainFrame,
				DlgMessageBox::eIcon_ExclamationWarning,
				_T("Error"),
				_T("Script not found"),
				_YTEXT(""),
				{ { IDOK, YtriaTranslate::Do(AutomationWizard_Run_3, _YLOC("OK")).c_str() } }).DoModal();
	}
}