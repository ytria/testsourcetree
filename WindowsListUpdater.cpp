#include "WindowsListUpdater.h"
#include "WindowsListObserver.h"

WindowsListUpdater& WindowsListUpdater::GetInstance()
{
	static WindowsListUpdater g_Instance;
	return g_Instance;
}

void WindowsListUpdater::Add(WindowsListObserver* p_pObserver)
{
	m_WindowsList.insert(p_pObserver);
}

void WindowsListUpdater::Remove(WindowsListObserver* p_pObserver)
{
	m_WindowsList.erase(p_pObserver);
}

void WindowsListUpdater::RemoveAll(WindowsListObserver* p_pObserver)
{
	m_WindowsList.clear();
}

void WindowsListUpdater::Update()
{
	for (const auto& pObs : m_WindowsList)
	{
		if(nullptr != pObs) 
			pObs->UpdateWindowList();
	}
}