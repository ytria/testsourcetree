#include "OnPremiseGroupsCollectionDeserializer.h"
#include "PSSerializeUtil.h"
#include "LoggerService.h"

void OnPremiseGroupsCollectionDeserializer::Deserialize(IPSObjectPropertyReader& p_Reader)
{
	OnPremiseGroup group;

	group.m_AdminCount = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("adminCount"));
	group.m_CanonicalName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("CanonicalName"));
	group.m_CN = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("CN"));
	group.m_Created = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("Created"));
	group.m_CreateTimeStamp = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("createTimeStamp"));
	group.m_Description = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Description"));
	group.m_DisplayName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("DisplayName"));
	group.m_DistinguishedName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("DistinguishedName"));
	group.m_DSCorePropagationData = PSSerializeUtil::DeserializeDateTimeCollection(p_Reader, _YTEXT("dSCorePropagationData"));
	group.m_GroupCategory = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("GroupCategory"));
	group.m_GroupScope = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("GroupScope"));
	group.m_GroupType = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("groupType"));
	group.m_HomePage = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("HomePage"));
	group.m_InstanceType = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("instanceType"));
	group.m_IsCriticalSystemObject = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("isCriticalSystemObject"));
	group.m_IsDeleted = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("isDeleted"));
	group.m_LastKnownParent = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("LastKnownParent"));
	group.m_ManagedBy = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ManagedBy"));
	group.m_Member = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("member"));
	group.m_MemberOf = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("MemberOf"));
	group.m_Members = PSSerializeUtil::DeserializeStringCollection(p_Reader, _YTEXT("Members"));
	group.m_Modified = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("Modified"));
	group.m_ModifyTimeStamp = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("modifyTimeStamp"));
	group.m_Name = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("Name"));
	group.m_ObjectCategory = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ObjectCategory"));
	group.m_ObjectClass = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ObjectClass"));
	group.m_ObjectGUID = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("ObjectGUID"));
	group.m_ObjectSid = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("objectSid"));
	group.m_ProtectedFromAccidentalDeletion = PSSerializeUtil::DeserializeBool(p_Reader, _YTEXT("ProtectedFromAccidentalDeletion"));
	group.m_SamAccountName = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("SamAccountName"));
	group.m_SAMAccountType = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("sAMAccountType"));
	group.m_SDRightsEffective = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("sDRightsEffective"));
	group.m_SID = PSSerializeUtil::DeserializeString(p_Reader, _YTEXT("SID"));
	group.m_SystemFlags = PSSerializeUtil::DeserializeInt32(p_Reader, _YTEXT("systemFlags"));
	group.m_USNChanged = PSSerializeUtil::DeserializeInt64AsTimeDate(p_Reader, _YTEXT("uSNChanged"));
	group.m_USNCreated = PSSerializeUtil::DeserializeInt64AsTimeDate(p_Reader, _YTEXT("uSNCreated"));
	group.m_WhenChanged = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("whenChanged"));
	group.m_WhenCreated = PSSerializeUtil::DeserializeDateTime(p_Reader, _YTEXT("whenCreated"));

	m_Data.push_back(group);
}

void OnPremiseGroupsCollectionDeserializer::SetCount(size_t p_Count)
{
	m_Data.reserve(p_Count);
}
