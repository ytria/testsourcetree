#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "InternetMessageHeader.h"

class InternetMessageHeaderDeserializer : public JsonObjectDeserializer, public Encapsulate<InternetMessageHeader>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

