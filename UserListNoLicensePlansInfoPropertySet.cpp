#include "UserListNoLicensePlansInfoPropertySet.h"
#include "UserListFullPropertySet.h"
#include "MsGraphFieldNames.h"
#include "O365AdminUtil.h"

vector<rttr::property> UserListNoLicensePlansInfoPropertySet::GetPropertySet() const
{
    UserListFullPropertySet fullPropSet;
    
    auto propSet = std::move(fullPropSet.GetPropertySet());
    Util::RemoveLicensePlansProperties(propSet);
    return propSet;
}

// =================================================================================================

vector<rttr::property> DeletedUserListNoLicensePlansInfoPropertySet::GetPropertySet() const
{
	auto propSet = UserListNoLicensePlansInfoPropertySet::GetPropertySet();

	// Same as UserListNoLicensePlansInfoPropertySet::GetPropertySet(), just add O365_USER_DELETEDDATETIME
	propSet.push_back(rttr::type::get<User>().get_property(O365_USER_DELETEDDATETIME));

	return propSet;
}
