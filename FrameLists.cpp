#include "FrameLists.h"

#include "AutomationNames.h"

IMPLEMENT_DYNAMIC(FrameLists, GridFrameBase)

//BEGIN_MESSAGE_MAP(FrameLists, GridFrameBase)
//END_MESSAGE_MAP()

FrameLists::FrameLists(const LicenseContext& p_LicenseContext, const SessionIdentifier& p_SessionIdentifier, const PooledString& p_Title, ModuleBase& module, HistoryMode::Mode p_HistoryMode, CFrameWnd* p_PreviousFrame)
	: GridFrameBase(p_LicenseContext, p_SessionIdentifier, p_Title, module, p_HistoryMode, p_PreviousFrame)
{
    m_CreateRefreshPartial = true;
}

void FrameLists::ShowLists(const O365DataMap<BusinessSite, vector<BusinessList>>& p_Lists, Origin p_Origin, bool p_FullPurge)
{
    if (CompliesWithDataLoadPolicy())
    {
		ASSERT(GetOrigin() == p_Origin);
        m_ListsGrid.BuildView(p_Lists, p_Origin, p_FullPurge);
    }
}

O365Grid& FrameLists::GetGrid()
{
    return m_ListsGrid;
}

void FrameLists::RefreshSpecific(AutomationAction* p_CurrentAction)
{
    CommandInfo info;
	info.SetOrigin(GetModuleCriteria().m_Origin);
	ASSERT(ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer);
	if (ModuleCriteria::UsedContainer::IDS == GetModuleCriteria().m_UsedContainer)
		info.GetIds() = GetModuleCriteria().m_IDs;
    info.SetFrame(this);
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
	CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Lists, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshAll, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameLists::RefreshIdsSpecific(const RefreshSpecificData& p_RefreshData, AutomationAction* p_CurrentAction)
{
    CommandInfo info;
    info.Data() = p_RefreshData;
    info.SetFrame(this);
    info.SetOrigin(GetOrigin());
	info.SetRBACPrivilege(GetModuleCriteria().m_Privilege);
    CommandDispatcher::GetInstance().Execute(Command(GetLicenseContext(), GetSafeHwnd(), Command::ModuleTarget::Lists, Command::ModuleTask::List, info, { &GetGrid(), g_ActionNameRefreshSelected, GetAutomationActionRecording(), p_CurrentAction }));
}

void FrameLists::createGrid()
{
    m_ListsGrid.Create(this, CRect(0, 0, 0, 0), AFX_IDW_PANE_FIRST);
}

GridFrameBase::O365ControlConfig FrameLists::GetRefreshPartialControlConfig()
{
	return GetRefreshPartialControlConfigSites();
}

bool FrameLists::customizeActionsRibbonTab(CXTPRibbonTab& tab, bool p_ShowLinkTabUser, bool p_ShowLinkTabGroup)
{
    GridFrameBase::customizeActionsRibbonTab(tab, false, false);

	auto systemGroup = tab.AddGroup(YtriaTranslate::Do(FrameLists_customizeActionsRibbonTab_1, _YLOC("System")).c_str());
	auto ctrl = systemGroup->Add(xtpControlButton, ID_LISTSGRID_TOGGLESYSTEMROWSVISIBILITY);
	GridFrameBase::setImage({ ID_LISTSGRID_TOGGLESYSTEMROWSVISIBILITY }, IDB_LISTS_TOGGLE_SYSTEMROWS, xtpImageNormal);
	GridFrameBase::setImage({ ID_LISTSGRID_TOGGLESYSTEMROWSVISIBILITY }, IDB_LISTS_TOGGLE_SYSTEMROWS_16X16, xtpImageNormal);
	setGridControlTooltip(ctrl);

    CXTPRibbonGroup* group = tab.AddGroup(g_ActionsLinkList.c_str());
    ASSERT(nullptr != group);
    if (nullptr != group)
    {
		setGroupImage(*group, 0);

		addNewModuleCommandControl(*group, ID_LISTSGRID_SHOWLISTITEMS, { ID_LISTSGRID_SHOWLISTITEMS_FRAME, ID_LISTSGRID_SHOWLISTITEMS_NEWFRAME }, { IDB_LISTS_SHOWLISTITEMS, IDB_LISTS_SHOWLISTITEMS_16X16 });
		addNewModuleCommandControl(*group, ID_LISTSGRID_SHOWCOLUMNS, { ID_LISTSGRID_SHOWCOLUMNS_FRAME, ID_LISTSGRID_SHOWCOLUMNS_NEWFRAME }, { IDB_LISTS_SHOWCOLUMNS, IDB_LISTS_SHOWCOLUMNS_16X16 });
    }

    return true;
}

AutomatedApp::AUTOMATIONSTATUS FrameLists::automationGetCommandIDorExecuteAction(UINT& p_CommandID, AutomationAction* p_Action)
{
	AutomatedApp::AUTOMATIONSTATUS statusRV = AutomatedApp::AUTOMATIONSTATUS_TRY;

	if (nullptr != p_Action)
	{
		const bool newFrame = p_Action->IsParamTrue(g_NewFrame);

		if (IsActionSelectedShowListItems(p_Action))
		{
			p_CommandID = newFrame ? ID_LISTSGRID_SHOWLISTITEMS_NEWFRAME : ID_LISTSGRID_SHOWLISTITEMS;
			Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
		}
		else if (IsActionSelectedShowListColumns(p_Action))
		{
			p_CommandID = newFrame ? ID_LISTSGRID_SHOWCOLUMNS_NEWFRAME : ID_LISTSGRID_SHOWCOLUMNS;
			Office365AdminApp::SetAutomationLastLoadingError(_YTEXT(""));
		}
		else if (IsActionShowSystemLists(p_Action))
		{
			m_ListsGrid.ShowSystemLists(true);
			setAutomationGreenLight(p_Action);
			statusRV = AutomatedApp::AUTOMATIONSTATUS_OK;
		}
		else if (IsActionHideSystemLists(p_Action))
		{
			m_ListsGrid.ShowSystemLists(false);
			setAutomationGreenLight(p_Action);
			statusRV = AutomatedApp::AUTOMATIONSTATUS_OK;
		}
	}

	return statusRV;
}