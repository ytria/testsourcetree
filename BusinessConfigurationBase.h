#pragma once

#include "ordered_set.h"

#define DECLARE_GET_INSTANCE(_Class) \
static const _Class& GetInstance() \
{ \
	static const _Class g_Instance; \
	return g_Instance; \
} \

class BusinessConfigurationBase
{
public:
	const wstring& GetTitle(const wstring& p_YUID) const;

	bool IsProperty4RoleDelegation(const wstring& p_YUID) const;
	bool IsComputedProperty(const wstring& p_YUID) const;

	const tsl::ordered_set<wstring>& GetProperties4RoleDelegation() const;
	
protected:
	BusinessConfigurationBase() = default;

	map<wstring, wstring>		m_YUIDandTitles;
	tsl::ordered_set<wstring>	m_Properties4RoleDelegation;
	set<wstring>				m_ComputedProperties;// non-graph properties
};
