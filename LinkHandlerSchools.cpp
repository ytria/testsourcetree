#include "LinkHandlerSchools.h"

#include "Command.h"
#include "CommandInfo.h"
#include "CommandDispatcher.h"

void LinkHandlerSchools::Handle(YBrowserLink& p_Link) const
{
	HistoryMode::SetCurrentMode(HistoryMode::NEW_HISTORY);

	CommandInfo info;
	info.SetOrigin(Origin::Tenant);
	info.SetRBACPrivilege(RoleDelegationUtil::RBAC_Privilege::RBAC_SITE_READ);
	CommandDispatcher::GetInstance().Execute(Command(p_Link.GetLicenseContext(LicenseContext::TokenRateType::SubModule), AfxGetApp()->m_pMainWnd->m_hWnd, Command::ModuleTarget::Schools, Command::ModuleTask::List, info, { nullptr, g_ActionNameShowSchools, nullptr, p_Link.GetAutomationAction() }));
}
