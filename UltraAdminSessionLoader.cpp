#include "DlgSpecialExtAdminUltraAdminJsonGenerator.h"
#include "UltraAdminSessionLoader.h"
#include "Office365Admin.h"
#include "OAuth2Authenticators.h"
#include "DlgLoading.h"
#include "ApplicationListRequester.h"
#include "DlgSpecialExtAdminAccess.h"
#include "UpdateApplicationRequester.h"
#include "RESTUtils.h"
#include "MSGraphCommonData.h"
#include "SessionTypes.h"
#include "YCallbackMessage.h"
#include "O365AdminUtil.h"
#include "SessionSwitcher.h"
#include "SessionIdsEmitter.h"
#include "SessionsSqlEngine.h"

using namespace std::literals::string_literals;

UltraAdminSessionLoader::UltraAdminSessionLoader(const SessionIdentifier& p_Id)
	: ISessionLoader(p_Id),
	m_IsFirstAttempt(false)
{
	ASSERT(p_Id.m_SessionType == SessionTypes::g_UltraAdmin);
}

TaskWrapper<std::shared_ptr<Sapio365Session>> UltraAdminSessionLoader::Load(const std::shared_ptr<Sapio365Session>& p_SapioSession)
{
	TaskWrapper<std::shared_ptr<Sapio365Session>> result = pplx::task_from_result(std::shared_ptr<Sapio365Session>());

	PersistentSession persistentSession = GetPersistentSession(p_SapioSession);
	Office365AdminApp& app = Office365AdminApp::Get();

	const wstring tenant = persistentSession.GetTenantName();
	const wstring appId = persistentSession.GetIdentifier().m_EmailOrAppId;
	ASSERT(persistentSession.GetTenantName().empty() || tenant == persistentSession.GetTenantName());
	ASSERT(persistentSession.GetAppId().empty() || appId == persistentSession.GetAppId());

	wstring appDisplayName = persistentSession.GetSessionName();

	// In case we got here right after having tried once, use the same password (the one saved in sessionInfo may have been overwritten).
	wstring appClientSecret = app.GetFullAdminAppPassword();
	if (appClientSecret.empty() && !persistentSession.GetAppClientSecret().empty())
		appClientSecret = persistentSession.GetAppClientSecret();

	if (!m_IsFirstAttempt)
	{
		DlgLoading::RunModal(_T("Querying application data..."), app.m_pMainWnd, [this, appId, p_SapioSession, &appDisplayName](const std::shared_ptr<DlgLoading>& p_DlgLoading) {
			auto application = ApplicationListRequester::GetAppFromAppId(p_SapioSession, appId);
			if (application.m_DisplayName)
				appDisplayName = *application.m_DisplayName;
			// FIXME: What if we didn't find the app?? we let appDisplayName empty so default generated name will be shown in DlgSpecialExtAdminAccess along with appID, which is wrong.
		});
	}

	DlgSpecialExtAdminAccess dlg(tenant, appId, appClientSecret, appDisplayName, app.m_pMainWnd, nullptr, std::make_unique<DlgSpecialExtAdminUltraAdminJsonGenerator>(true), true);
	bool load = m_IsFirstAttempt;
	if (!m_IsFirstAttempt)// do not show dlg upon first attempt
	{
		load = dlg.DoModal() == IDOK;
		if (load && appDisplayName != dlg.GetDisplayName())
		{
			DlgLoading::RunModal(_T("Querying info"), nullptr, [p_SapioSession, appId, newName = dlg.GetDisplayName()](const std::shared_ptr<DlgLoading>& p_Dlg) {
				auto application = ApplicationListRequester::GetAppFromAppId(p_SapioSession, appId);
				ASSERT(application.m_Id);
				if (application.m_Id)
				{
					auto updateRequester = std::make_shared<UpdateApplicationRequester>(*application.m_Id);
					updateRequester->SetDisplayName(newName);
					updateRequester->Send(p_SapioSession, YtriaTaskData()).GetTask().wait();
				}
				else
				{
					// FIXME: Show error?
				}
			});
		}
	}

	if (load)
	{
		ASSERT(dlg.GetAppId() == p_SapioSession->GetEmailOrAppId());
		app.SetFullAdminAppId(dlg.GetAppId());
		app.SetFullAdminTenant(dlg.GetTenant());
		app.SetFullAdminAppPassword(dlg.GetSaveSecretKey() ? dlg.GetClientSecret() : _YTEXT(""));
		app.SetFullAdminDisplayName(dlg.GetDisplayName());

		web::http::uri_builder tokenUri(Rest::GetAzureADEndpoint());
		tokenUri.append_path(tenant);
		tokenUri.append_path(_YTEXT("oauth2/v2.0/token"));

		p_SapioSession->GetMainMSGraphSession()->SetAuthenticator(std::make_unique<ClientCredentialsAuthenticator>(p_SapioSession->GetMainMSGraphSession(),
			dlg.GetAppId(),
			dlg.GetClientSecret(),
			tokenUri.to_string(),
			Rest::GetMsGraphEndpoint() + _YTEXT("/.default"))
		);

		SessionIdentifier sessionBeingLoadedId;
		sessionBeingLoadedId.m_EmailOrAppId = dlg.GetAppId();
		sessionBeingLoadedId.m_SessionType = SessionTypes::g_UltraAdmin;
		sessionBeingLoadedId.m_RoleID = 0;

		if (!app.GetSessionBeingLoadedIdentifier())
			app.SetSessionBeingLoadedIdentifier(sessionBeingLoadedId);

		const wstring oldSessionType = app.GetSessionType();
		app.SetSessionType(SessionTypes::g_UltraAdmin);

		result = p_SapioSession->GetMainMSGraphSession()->Start(app.m_pMainWnd).Then([oldSessionType, p_SapioSession, sessionId = persistentSession.GetIdentifier(), isFirstAttempt = m_IsFirstAttempt](const RestAuthenticationResult& p_Result)
		{
			std::shared_ptr<Sapio365Session> session;
			bool success = p_Result.state == RestAuthenticationResult::State::Success;
			auto& app = Office365AdminApp::Get();
			if (!success)
			{
				YCallbackMessage::DoPost([isFirstAttempt, sessionId, error = p_Result.error_message]()
				{
					auto& app = Office365AdminApp::Get();
					TraceIntoFile::trace(_YDUMP("Office365AdminApp"), _YDUMP("Load Session"), _YDUMPFORMAT(L"Error while authenticating %s %s: %s", SessionTypes::g_UltraAdmin.c_str(), sessionId.m_EmailOrAppId.c_str(), error.c_str()));

					if (AutomatedApp::IsAutomationRunning() && IsActionLoadSession(app.GetCurrentAction()))
					{
						app.GetCurrentAction()->AddError(_YFORMAT(L"An error occurred while authenticating: %s", Sapio365Session::ParseGraphJsonError(error).m_ErrorDescription.c_str()).c_str());
						app.SetAutomationGreenLight(app.GetCurrentAction());
					}
					else
					{
						if (!isFirstAttempt)
							Util::ShowApplicationConnectionError(_T("Unable to sign in using the provided Ultra Admin credentials."), error, app.m_pMainWnd);

						auto newLoader = std::make_unique<UltraAdminSessionLoader>(sessionId);
						newLoader->SetIsFirstAttempt(false);
						SessionSwitcher switcher(std::move(newLoader));
						switcher.Switch();
					}
				});

				std::lock_guard<std::mutex> lock(app.GetSessionTypeLock());
				app.SetSessionType(oldSessionType);
			}
			else
			{
				TraceIntoFile::trace(_YDUMP("[["s) + SessionIdsEmitter(sessionId).GetId() + _YDUMP("]]") + _YDUMP("Office365AdminApp"), _YDUMP("LoadSession"), _YDUMPFORMAT(L"Successfully loaded %s session with app id %s", app.GetSessionType().c_str(), sessionId.m_EmailOrAppId.c_str()));
				session = p_SapioSession;
			}

			return session;
		});
	}
	else
	{
		result = pplx::task_from_result(std::shared_ptr<Sapio365Session>());
		// Make sure we don't keep the previous password in memory (see "wstring appClientSecret = GetFullAdminAppPassword();" above)
		app.SetFullAdminAppPassword(_YTEXT(""));

		if (AutomatedApp::IsAutomationRunning())
		{
			if (IsActionLoadSession(app.GetCurrentAction()))
				app.SetActionCanceledByUser(app.GetCurrentAction(), TraceOutput::TRACE_ERROR);
		}
	}

	return result;
}

void UltraAdminSessionLoader::SetIsFirstAttempt(bool p_IsFirstAttempt)
{
	m_IsFirstAttempt = p_IsFirstAttempt;
}
