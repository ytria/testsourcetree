#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "ResourceGroup.h"

namespace Azure
{
	class ResourceGroupDeserializer : public JsonObjectDeserializer, public Encapsulate<Azure::ResourceGroup>
	{
	public:
		void DeserializeObject(const web::json::object& p_Object) override;
	};
}
