#pragma once

#include "JsonObjectDeserializer.h"
#include "Encapsulate.h"
#include "PersonOrGroupColumn.h"

class PersonOrGroupColumnDeserializer : public JsonObjectDeserializer, public Encapsulate<PersonOrGroupColumn>
{
protected:
    virtual void DeserializeObject(const web::json::object& p_Object) override;
};

